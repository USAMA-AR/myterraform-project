resource "aws_instance" "r100c96" {
  ami               = "ami-0f19d220602031aed"
  instance_type     = "t2.micro"
  availability_zone = "us-east-2a"
  key_name          = "UbuntuKey"
  security_groups = ["${aws_security_group.web-node.name}"]
  tags = {
    Name = "work_Terraform"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.r100c96.private_ip} > inventory"
  }
  provisioner "remote-exec" {
    inline = [ "sudo hostnamectl set-hostname cloudEc2.technix.com" ]
    connection {
      host        = aws_instance.r100c96.public_dns
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("UbuntuKey.pem")
    }
  }
}


output "ip" {
  value = aws_instance.r100c96.public_ip
}

output "publicName" {
  value = aws_instance.r100c96.public_dns
}
